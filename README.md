# www.birdcalls.is

## Transparency of evolution

This repo is to track the changes to the www.birdcalls.is website.

It is our commitment to provide a public record of the evolution of the text content for our website as we evolve. This git repository tracks our internal development of the website so that we have a record of the commitments we've made to the community at large.
